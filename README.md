# Bruce Banner

bruce banner is an IP banning tool. It manages a list of IPs to ban and
communicates bans to a network of remote server responsible for enforcing the
ban via `nftables`.

It's built for a specific use case: a network of servers all send their logs to
a central logging tool. The central logging tool is monitored for abusive
behavior and, when detected, abusive IPs are relayed back to the network of
servers to be banned.

For example, if you send your logs via
[filebeat](https://www.elastic.co/guide/en/beats/filebeat/current/index.html)
to an [elasticsearch](https://www.elastic.co/products/elasticsearch) instance
you can use `bruce-elastic` to continuously query elasticsearch for IPs to ban.
When it finds IPs, it sends them to `bruce-server` which, in turn, distributes
the bans to all the `bruce-banner` clients.

If you don't use elasticsearch or have your own elasticsearch monitoring tool,
your own tool can run `bruce ban <ip> <topic>` and bruce will take care of
managing it and distributing it to your network.

## Topics

`bruce` can be configured to manage different ban "topics" for different
services. For example, some servers can subscribe to the "ssh" topic which
triggers a ban on just port 22 or a "web" topic and ban ports 80 and 443. You
can specify any number of topic port combinations.

## General architecture

bruce banner runs in a client server configuration:

 * bruce-server: the server component opens two sockets:

    1. It listens via 127.0.0.1 on port 5556 (by default) where it runs the
       [zeromq](https://zeromq.org/) PUB part of a PUB-SUB service.

    2. It connects to a unix socket via `/run/bruce/bruce.sock` where it listens
     for commands from the `bruce` command line tool.

 * bruce-banner: the client component runs and connects to the server component
   via zeromq and waits for information on IPs to ban.

    The bruce-banner client communicates with the bruce-server via an ssh proxy.
    It must be configured with password-less ssh access to the bruce-transport
    user on the server running bruce-server.

 * bruce: additionally, there is a command line program that controls the
   bruce-server. It can be used to add IPs to the list of IPs to ban, remove
   IPs (e.g. `bruce add 1.2.3.4 ssh` or `bruce remove 1.2.3.4 ssh`).

 * bruce-elastic: lastly, there is an optional daemon specifically designed to
   query an elasticsearch database for IPs to send to `bruce-server`. See below
   for more information about how it works.

## Typical sequence of events

It doesn't matter in what order the clients and servers start.

Typically:

1. Start the bruce-server

2. Start program that tails logs and generates bans by calling `bruce add <ip>
<topic>` (or start `bruce-elastic`)

3. bruce-server records the ban, ban time and date it should expire in the
dataase and broadcasts it to al subscribers to the topic.

4. New client starts the `bruce-banner` program. The program starts by
requesting a sync of all current bans for it's chosen bans and bans them.

5. Then, `bruce-banner` continues listening for new ones.

## Permissions

The server component should run as the `bruce-server` user and will have full
access to the database of IPs.

A group called `bruce` is granted write access to the `bruce-server`'s unix
socket. Any user that will be communicating IPs to ban should be in this group
(e.g. the `elastalert` user or the `bruce-elastic` user).

Lastly, a `bruce-transport` user should be created on the `bruce-server` machine
and configured to allow all `bruce-banner` clients ssh access so they can connect
to the server. The `bruce-transport` user does not have access to run the `bruce`
command or the `/run/bruce/bruce.sock` so they can't add or remove IPs. However,
`bruce-transport` should be given access via sudo to run `bruce sync *` so it can
trigger the server to send it all bans it may have missed while it was not
running.

## Ignore list

With any IP banning tool it's useful to have a list of IP addresses that should
never be banned.

Unfortunately, it's hard to maintain this list.

With bruce, you can define a list of IPs to ban via three methods and bruce
will take care of keeping the list up to date:

 * domain name: The IPs that the domain name resolves to will be ignored
 * web site: All IPs listed on a web site will be ignored (supports password protected web sites)
 * static: a static list of single IPs or IPs listed in CIDR notation can also be designated.

This configuration is specified in a yaml formatted file and you can call
`bruce refresh-ignore` to update it via a systemd timer as often as you like.

## Banning

bruce bans using `nftables`. Specifically, it creates an [nftables
set](https://wiki.nftables.org/wiki-nftables/index.php/Sets), defines a rule
for that set specifying which ports should be blocked, and then simply adds IPs
to that set with a timeout value for when they should be automatically removed.

## Elasticsearch integration

Optionally, the `bruce-elastic` service can be used to query your elasticsearch
database for patterns that should trigger a ban. You can place yaml files in
the `/etc/bruce-elastic-rules.d` directory that describe your rules. See the
`bruce-elastic-rules.d` directory for examples. The sample.yml.disabled rule
has full documentation of all the options.

While `bruce-elastic` is designed to run as a service, you can also run it as a
one time command to test rules, with the syntax:

    BRUCE_ELASITC_HOST=https://your.elastic.search.server/ BRUCE_ELASTIC_USER=username BRUCE_ELASTIC_PASSWORD=secret bruce-elastic --test /path/to/rule.yml

To use bruce-elastic, create the symlink:

 * `ln -s /usr/local/src/bruce/bruce-elastic /usr/local/sbin/bruce_elastic.py`

## Install

### Check out the code

On each host, checkout the code into `/usr/local/src/bruce`.

### On the logging host

On the server that has access to your centralized logs:

Install: `apt install python3-urllib3 python3-ipy python3-dns python3-yaml sudo`

And... `pip3 install pyzmq`

Create the following symlinks:

 * `ln -s /usr/local/src/bruce/bruce-server /usr/local/sbin/bruce_server.py`
 * `ln -s /usr/local/src/bruce/bruce /usr/local/bin/bruce.py`

Then, create these users:

 * bruce-server: `adduser --system --home /var/lib/bruce-server bruce-server`
 * bruce-refresh-ignore: `adduser --system --home /var/lib/bruce-server bruce-refresh-ignore`
 * bruce-transport: `adduser --home /var/lib/bruce-transport bruce-transport`

And the group bruce: `group add bruce`

Add bruce-refresh-ignore and bruce-server to the bruce group: `adduser bruce-refresh-ignore bruce; adduser bruce-server bruce`

Copy the bruce-server.sudoers file to `/etc/sudoers.d/bruce-server`.

Make the bruce configuration directory: `mkdir /etc/bruce`

Copy the bruce-ignore.yml file to /etc/bruce: `cp bruce-ignore.yml /etc/bruce/bruce-ignore.yml`

Edit /etc/bruce/bruce-ignore.yml to your liking.

Add the following files to /etc/systemd/system:

 * systemd/bruce-server.service
 * systemd/bruce-refresh-ignore.service
 * systemd/bruce-refresh-ignore.timer

Enable and start the server: `systemctl enable bruce-server && systemctl start bruce-server`
Enable and start the refresh ignore timer: `systemctl enable bruce-refresh-ignore.timer && systemctl start bruce-refresh-ignore.timer`

### On the banning hosts

On each server that will be banning traffic:

Install: `apt install python3-ipy sudo nftables`

And... `pip3 install pyzmq`

Create the following symlink:

 * `ln -s /usr/local/src/bruce/bruce-banner /usr/local/sbin/bruce_banner.py`

Then, create the bruce-banner user: `adduser --system --home=/var/lib/bruce-banner bruce-banner`

Generate an ssh key pair without a password: `su - ssh-keygen bruce-banner`

Add the contents of the `/var/lib/bruce-banner/.ssh/id_rsa.pub` to the `/var/lib/bruce-transport/.ssh/authorized_keys` file *on the bruce-server host*.

Copy the bruce-banner.sudoers file to `/etc/sudoers.d/bruce-banner`.

Copy systemd/etc-default-bruce-banner to /etc/default/bruce-banner and edit to your liking.

Copy systemd/bruce-banner.service systemd/bruce-banner-ssh-proxy.service  to /etc/systemd/system/

Enable and start the services: `systemctl enable bruce-banner-ssh-proxy && systemctl start bruce-banner-ssh-proxy && systemctl enable bruce-banner && systemctl start bruce-banner`

## Tests

A few simple tests are included to make sure everything works properly.

To use them, you have to install the `python3-mock` class.

Then:

    python3 -m unittest tests/test.py

You can also manually test in debug mode (which can be run without root/sudo
setup). You need three terminal sessions:

  1. `BRUCE_DEBUG=1 BRUCE_ACCESS_GROUP="$(groups|awk '{print $1}')" ./bruce_server.py`
  2. `BRUCE_DEBUG=1 ./bruce_banner.py`
  3. `./bruce.py add 192.168.1.1 ssh`
