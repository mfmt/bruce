#!/usr/bin/python3

# bruce maintains the database of banned IPs and communicates with the servers
# that will be executing the actual bans. bruce is a command line program.

import sys
import os
import socket
import datetime

def usage():
    print("""
bruce - ip ban manager
  bruce sends requests to bruce-server.

Usage
  bruce <subcommand> [arguments]

Subcommands
    add <ip> <topic> - ban the given ip
    remove <ip> <topic> - remove the ban
    refresh-ignored - refresh ignored IPs
    is-banned <ip> - report ip ban status of given IP
    is-ignored <ip> - report if given IP is being ignored

Topics
    Which service should be banned (e.g. web, ssh, mail)

Loading ignored IPs
    You can generate a list of IPs to ignore from a yaml configuration file
    that defines domains, static IPs and web sites with lists of IPs. Running
    this command periodically ensures that your ignore list remains fresh.

Environment Variables
    BRUCE_SERVER_SOCKET_PATH
    BRUCE_SERVER_DB_TYPE (sqlite3, mysql)
    BRUCE_SERVER_SQLITE_PATH
    BRUCE_SERVER_MYSQL_HOST
    BRUCE_SERVER_MYSQL_USER
    BRUCE_SERVER_MYSQL_DB_NAME
    BRUCE_SERVER_MYSQL_PASSWORD
""")

def send(cmd):
    # First look for sock based on environment variable or in current directory
    # (convenient for developing).
    socket_path = os.getenv('BRUCE_SERVER_SOCKET_PATH', 'bruce.sock')
    if not os.path.exists(socket_path) is True:
        # Next try the default server location.
        socket_path = '/run/bruce/bruce.sock'
        if not os.path.exists(socket_path) is True:
            print("Can't connect to bruce-server. Is it running?")
            exit(1)

    client = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    try:
        client.connect(socket_path)
    except ConnectionRefusedError:
        print("Can't connect to bruce-server. Is it running?")
        exit(1)
    client.send(cmd.encode('utf-8'))
    while True:
        datagram = client.recv(1024)
        if not datagram:
            print("Empty response from server, something went wrong.")
        else:
            response = datagram.decode('utf-8')
            if response != 'OK':
                print("Error: {0}".format(response))
        break

def main(action):
    if action == 'add':
        # IP and topic are required arguments.
        try:
            ip = sys.argv[2]
            topic = sys.argv[3]
        except IndexError:
            print("Please pass IP and topic as second and third arguments.")
            usage()
            exit(1)
        send("add {0} {1}".format(ip, topic))
    elif action == 'remove':
        # IP and topic are required...
        try:
            ip = sys.argv[2]
        except IndexError:
            print("Please pass IP to remove.")
            usage()
            exit(1)
        try:
            topic = sys.argv[3]
        except IndexError:
            print("Please pass topic to remove.")
            usage()
            exit(1)
        send("remove {0} {1}".format(ip, topic))
    elif action == 'refresh-ignore':
        send("refresh-ignore")
    elif action == 'sync':
        # Both topic and the bruce-banner host to sync are required.
        try:
            target = sys.argv[2]
            topic = sys.argv[3]
        except IndexError:
            print("Please pass the short hostname of the server to sync (e.g. web-001) followed by the topic to sync (e.g. ssh, web).")
            usage()
            exit(1)

        send("sync {0} {1}".format(target, topic))
    elif action == 'is-banned' or action == 'is-ignored':
        # Both is-banned and is-ignored require an IP address.
        try:
            ip = sys.argv[2]
        except IndexError:
            print("Please pass IP as second argument.")
            usage()
            exit(1)

        # These require direct access to the database, to which we may or may
        # not have permission.
        #
        # Start by trying to read the /etc/default/bruce-server file which would
        # typically load the db credentials as environment variables.
        try:
            with open('/etc/default/bruce-server') as f:
                 for line in f:
                    line = line.strip()
                    if line and not line.startswith('#'):
                        parts = line.split('=')
                        if len(parts) == 2:
                            key = parts[0].strip()
                            value = parts[1].strip()
                            os.environ[key] = value
        except FileNotFoundError:
            # If there is no file, continue, maybe environment is set
            # some other way.
            pass
        except PermissionError:
            # Also ok, in theory, environment could still be set
            # some other way
            pass

        try:
            db_type = os.getenv("BRUCE_SERVER_DB_TYPE", 'sqlite3')
            if db_type == 'sqlite3':
                import sqlite3
                sqlite_path = os.getenv('BRUCE_SERVER_SQLITE_PATH', 'db.sqlite3')
                db = sqlite3.connect(sqlite_path)
                print("Connecting to sqlite database {0}".format(sqlite_path))
            elif db_type == 'mysql':
                import pymysql
                connection_params = {
                    'host': os.getenv('BRUCE_SERVER_MYSQL_HOST', 'localhost'),
                    'db': os.getenv('BRUCE_SERVER_MYSQL_DB_NAME', 'bruce'),
                    'user': os.getenv('BRUCE_SERVER_MYSQL_USER', 'bruce'),
                    'password': os.getenv('BRUCE_SERVER_MYSQL_PASSWORD', None),
                    'unix_socket': os.getenv('BRUCE_SERVER_MYSQL_UNIX_SOCKET', '/run/mysqld/mysqld.sock')
                }

                db = pymysql.connect(**connection_params)
                print("Connecting to mysql database {0} on {1}".format(connection_params['db'], connection_params['host']))
            db_cursor = db.cursor()
        except:
            print("Failed to connect to database. May you don't have access?")
            sys.exit(1)

        if action == 'is-ignored':
            db_cursor.execute(set_ph("SELECT ip FROM ignored WHERE ip = :ph"), (ip,))
            row = db_cursor.fetchone()
            if not row:
                print("{0}: NOT ignored".format(ip))
            else:
                print("{0}: IS ignored".format(ip))
        elif action == 'is-banned':
            db_cursor.execute(set_ph("SELECT ban_date, expire_date, topic, ban_count, unban_date FROM banned WHERE ip = :ph"), (ip,))
            rows = db_cursor.fetchall()
            if not rows:
                print("{0}: NOT banned".format(ip))
            else:
                for row in rows:
                    ban_date = datetime.datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S.%f')
                    expire_date = datetime.datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S.%f')
                    topic = row[2]
                    ban_count = row[3]
                    unban_time = row[4]
                    print(f"Topic: {topic}")
                    if unban_time:
                        unban_date = datetime.datetime.strptime(unban_time, '%Y-%m-%d %H:%M:%S.%f')
                    else:
                        unban_date = datetime.datetime.fromtimestamp(0)

                    status_banned = False
                    status_unbanned = False
                    if expire_date > datetime.datetime.now():
                        status_banned = True
                    if unban_date > datetime.datetime.now():
                        status_unbanned = True

                    if status_banned and status_unbanned:
                        print(f"  {ip}: NOT banned (would be banned but has been unbanned)")
                    elif not status_banned and status_unbanned:
                        print(f"  {ip}: NOT banned (ban has expired, unbanned still in effect)")
                    elif status_banned and not status_unbanned and unban_time:
                        print(f"  {ip}: BANNED (unban set, but expired)")
                    elif status_banned and not status_unbanned and not unban_time:
                        print(f"  {ip}: BANNED")

                    print(f"  Last banned on: {ban_date.strftime('%Y-%m-%d %H:%M:%S')}")
                    if unban_time:
                        print(f"  Unban (expiration): {unban_date.strftime('%Y-%m-%d %H:%M:%S')}")
                    print(f"  Expire: {expire_date.strftime('%Y-%m-%d %H:%M:%S')}")
                    print(f"  Count: {ban_count}")
    else:
        print("I haven't implemented the sub command {0}.".format(action))
        usage()
        exit(1)

# Mysql and sqlite use different placeholders. Sheesh. We have to run all queries
# through this silliness to maintain compatibility between mysql and sqlite.
# This is ridiculous. https://stackoverflow.com/questions/12184118/python-sqlite3-placeholder
def set_ph(sql):
    db_type = os.getenv("BRUCE_SERVER_DB_TYPE", 'sqlite3')
    if db_type == 'sqlite3':
        placeholder = '?'
    else:
        placeholder = '%s'
    ret = sql.replace(':ph', placeholder)
    return ret

try:
    action = sys.argv[1]
    main(action)
except IndexError:
    print("A subcommand is required as the first argument.")
    usage()
    exit(1)


