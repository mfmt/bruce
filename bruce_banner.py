#!/usr/bin/python3

# bruce-banner is intended to be run via systemd. It is constantly listening
# for a message from bruce - which will prompt it to ban or unban an IP.
#
# Technically, brue-banner doesn't ban or unban anything, it simply adds or
# removes IPs from the set called bruce-<topic>. When started, it initializes
# that set, along with the ban rules that should be applied.

import zmq
import sys
import os
import subprocess
import re
from IPy import IP
import socket
import signal
import tempfile
import datetime

class BruceBanner():
    topic_ports = {}
    mq_socket = None
    hostname = None
    last_sync = None

    def __init__(self):
        signal.signal(signal.SIGHUP, self.handle_hup)
        signal.signal(signal.SIGTERM, self.handle_term)

    def handle_term(self, signum, frame):
      self.log("Received TERM")
      self.cleanup()
      sys.exit(0)

    # If changes are made to the BRUCE_BANNER_SUBSCRIBE_TOPICS setting in
    # /etc/default/bruce-banner you can reload by sending a HUP signal (or
    # systemctl reload bruce-banner). This will update the ports being blocked,
    # but not touch the list of banned IPs. If you want to reload the list of
    # banned IPs, then you should restart bruce-banner.
    def handle_hup(self, signum, frame):
        self.log("Received HUP.")
        default_file = '/etc/default/bruce-banner'
        if not os.path.exists(default_file):
            self.log("Environment file not found at {0}, not reloaded.".format(default_file))
            return

        new_subscribe_topics = None
        with open(default_file) as f:
            lines = f.readlines()
            for line in lines:
                if line.startswith("BRUCE_BANNER_SUBSCRIBE_TOPICS="):
                    pieces = line.strip().split('=')
                    if len(pieces) == 2:
                        new_subscribe_topics = pieces[1].strip('"\'')
        if not new_subscribe_topics:
            self.log("Loaded configuration file but did not find a value for the BRUCE_BANNER_SUBSCRIBE_TOPICS setting.")
            return

        os.environ['BRUCE_BANNER_SUBSCRIBE_TOPICS'] = new_subscribe_topics
        self.log("New BRUCE_BANNER_SUBSCRIBE_TOPICS have been loaded: {0}.".format(os.getenv('BRUCE_BANNER_SUBSCRIBE_TOPICS', '')))
        try:
            self.parse_subscribe_topics()
        except RuntimeError as e:
            # Catch all errors - we don't want to crash and burn if there is a
            # typo in the defaults file.
            self.log("Failed to reload. Error: {0}".format(e))
            return

        self.initialize_nftables()
        self.log("Successfully reloaded.")

    def log(self, msg):
        print(msg, flush=True)

    def execute(self, args):
        debug = os.getenv('BRUCE_DEBUG', 0)
        if debug:
            if "--file" in args:
                # We are using the --file option to nft, which means that we
                # are executing an nft file. That means we can pass "--check"
                # and it will throw an error if our syntax is off.
                new_args = []
                for arg in args:
                    # Since we might be a non-root user, we also have to ensure
                    # we have the full path and we have to remove the initial
                    # sudo.
                    if arg == "sudo":
                        continue
                    if arg == "nft":
                        arg = "/usr/sbin/nft"
                    new_args.append(arg)
                new_args.append("--check")
                result = subprocess.run(new_args, capture_output=True, text=True)
                # If we run as a non-privileged user, it will always have
                # result.returncode set to 1 (even if we pass --check, which
                # seems wrong).  But, it does output an error to stderr if
                # there is a problem and outputs nothing if the syntax is good,
                # so that is what we check.
                if result.stderr:
                    # Gah. Now we seem to get a single line error if we don't have permission
                    # so let's ignore that.
                    if result.stderr != 'netlink: Error: cache initialization failed: Operation not permitted\n':
                        raise RuntimeError("Failed to parse nft file: '{0}'.".format(result))
                args = new_args
            self.log(args)
        else:
            self.log(args)
            subprocess.run(args)

    def setup(self):
        connect_port = os.getenv('BRUCE_BANNER_CONNECT_PORT', '5555')
        connect_ip = os.getenv('BRUCE_BANNER_CONNECT_IP', '127.0.0.1')
        self.hostname = os.getenv('BRUCE_BANNER_HOSTNAME', socket.gethostname())

        # Socket to talk to server
        context = zmq.Context()
        self.mq_socket = context.socket(zmq.SUB)

        # Unfortunately, ssh.tunnel doesn't seem to work with the pub/sub model, the
        # connection is closed immediately. Anyway, I would prefer to control the ssh
        # tunnel using ssh via the systemd ExecStartPre and Post commands.
        #ssh.tunnel.tunnel_connection(socket, "tcp://{0}:{1}".format(connect_ip, connect_port), server, timeout=60000)
        self.mq_socket.connect("tcp://{0}:{1}".format(connect_ip, connect_port))

        self.parse_subscribe_topics()

        for topic in self.topic_ports:
            self.log("Subscribing to {0}".format(topic))
            self.mq_socket.setsockopt_string(zmq.SUBSCRIBE, topic)

            # If we can learn the hostname, subscribe to hostname:topic so the server
            # can synchronize this instance when it starts.
            if self.hostname:
                hostname_topic = "{0}:{1}".format(self.hostname, topic)
                self.log("Subscribing to {0}".format(hostname_topic))
                self.mq_socket.setsockopt_string(zmq.SUBSCRIBE, hostname_topic)

        self.initialize_nftables()
        self.last_sync = datetime.datetime.now()
        self.request_sync()

    def parse_subscribe_topics(self):
        subscribe_topics = os.getenv('BRUCE_BANNER_SUBSCRIBE_TOPICS', 'ssh:22 web:80,143 mail:110,143,465,587,993,995')
        self.topic_ports = {}
        for topic_pairs in subscribe_topics.split():
            # First, validate.
            topic, ports = topic_pairs.split(':')
            if not re.match('^[a-z0-9]+$', topic):
                raise RuntimeError("Invalid SUBSCRIBE_TOPIC setting. Topics can only have lowercase letters and numbers, not {0}.".format(topic))
            self.topic_ports[topic] = []
            for port in ports.split(','):
                if not re.match('^[0-9]+$', port):
                    raise RuntimeError("Invalid SUBSCRIBE_TOPIC setting. Ports can only have numbers, not {0}.".format(port))
                self.topic_ports[topic].append(port)

            if len(self.topic_ports[topic]) == 0:
                raise RuntimeError("Invalid SUBSCRIBE_TOPIC setting. No ports defined for topic {0}".format(topic))

        if not self.topic_ports:
            raise RuntimeError("Invalid SUBSCRIBE_TOPIC setting. No topics are defined.")

    def initialize_nftables(self):
        # Initialize the nftables. This command is conveniently idempotent.
        args = [ 'sudo', 'nft', 'add', 'table', 'inet', 'bruce' ]
        self.execute(args)

        # This one initializes an input chain (named hulk) to hold the rules
        # (also idempotent):
        args = [ 'sudo', 'nft', 'add', 'chain', 'inet', 'bruce', 'hulk',  '{ type filter hook input priority 0; }' ]
        self.execute(args)

        # Flush existing rules. If we don't flush, we get duplicate rules.
        # Note: this does not clear the existing set. That only happens if we
        # sync (and call nft_set to replace the set of banned IPs).
        args = [ 'sudo', 'nft', 'flush', 'chain', 'inet', 'bruce', 'hulk' ]
        self.execute(args)
        # Now, add the rules.
        for topic in self.topic_ports:
            args = [ 'sudo', 'nft', 'set', 'inet', 'bruce', 'bruce-{0}'.format(topic), '{ type ipv4_addr; flags timeout; }' ]
            self.execute(args)
            args = [ 'sudo', 'nft', 'add', 'rule', 'inet', 'bruce', 'hulk', 'ip', 'saddr', '@bruce-{0}'.format(topic), 'tcp', 'dport', '{{ {0} }}'.format(','.join(self.topic_ports[topic])), 'drop' ]
            self.execute(args)

    # Request that the banner-server send us a current list of all banned
    # IPs for each of our subscriptions.
    def request_sync(self):
        proxy_login = os.getenv('BRUCE_SSH_PROXY_LOGIN', None)
        if self.hostname and proxy_login:
            for topic in self.topic_ports:
                args = [
                    "ssh",
                    proxy_login,
                    "sudo",
                    "bruce",
                    "sync",
                    self.hostname,
                    topic
                ]
                self.execute(args)

    # Replace the entire set of banned IPS for a given topic with a
    # new set.
    def nft_set(self, topic, ban_set):
        # If the set is empty do nothing.
        if not ban_set:
            return

        # The set might be too big to pass as an argument to the nft command, so we have to
        # create a nft program to execute. We can't delete the file - we need to flush and
        # close so the nft command has access to it.
        with tempfile.NamedTemporaryFile(mode='w', delete=False) as temp_file:
            # Flush the rule set - this ensures that we have exactly the same
            # rules as everyone else. Also, it's important to do this from
            # within the nft file so it's atomic - we don't want to flush the
            # set and then fail to re-load it due to a syntax error.
            temp_file.write("flush set inet bruce bruce-{0};".format(topic))
            temp_file.write("add set inet bruce bruce-{0} {{ type ipv4_addr; flags timeout;".format(topic))
            temp_file.write("  elements = {")
            temp_file.write(ban_set)
            temp_file.write("  };")
            temp_file.write("}")
            temp_file.flush

        # Now execute.
        self.execute(["sudo", "nft", "--file", temp_file.name])
        os.remove(temp_file.name)

    # Add or remove an IP address from the existing set.
    def nft_single(self, action, ip, topic, timeout = 0):
        if action == 'ban':
            command = 'add'
            elements = "{{ {0} timeout {1} }}".format(ip, timeout)
        elif action == 'unban':
            command = 'delete'
            elements = "{{ {0} }}".format(ip)

        set_name = "bruce-{0}".format(topic)
        args = [
            "sudo",
             "nft",
             command,
             "element",
             "inet",
             "bruce",
             set_name,
             elements
        ]
        self.log("Banning {0} on IP {1} for {2}".format(topic, ip, timeout))
        self.execute(args)

    def cleanup(self):
        args = [ 'sudo', 'nft', 'delete', 'table', 'inet', 'bruce' ]
        self.execute(args)

    def listen(self):
        try:
            while True:
                string = self.mq_socket.recv()
                pieces = string.decode('utf-8').split()

                topic = str(pieces[0])
                # The topic might be prefaced with our hostname. If so, remove the hostname.
                topic = topic.replace(self.hostname + ':', '')
                action = str(pieces[1])

                # Sanitize our input.
                if action != 'ban' and action != 'unban' and action != 'sync':
                    self.log("Action should be ban or unban or sync.")
                    continue

                if topic not in self.topic_ports:
                    self.log("We should not be subscribed to topic {0}. Ignoring.".format(topic))
                    continue

                if action == "sync":
                  # Special case. This means the rest of the message is the full
                  # set.
                  ban_set = str(" ".join(pieces[2:]))
                  self.nft_set(topic, ban_set)
                else:
                  ip = str(pieces[2])
                  timeout = str(pieces[3])

                  try:
                      # Try to parse the IP to test if it's a legit IP
                      IP(ip)
                  except:
                      # Don't fail if we fail to parse the IP, just ignore this one.
                      self.log("Failed to parse IP {0}".format(ip))
                      continue

                  # If action is ban, validate the timeout. Should be in
                  # format 2d3h40m6s First, make sure we have at least one
                  # number and one time spec. If action is unban, we ignore
                  # the timeout.
                  if action == 'ban':
                      if not re.match('[0-9]+[dhms]', timeout):
                          self.log("Failed to parse time out {0}. Must have at least one number and one of d, m, h, s".format(timeout))
                          continue
                      if not re.match('^([0-9]+d)?([0-9]+h)?([0-9]+m)?([0-9]+s)?$', timeout):
                          self.log("Failed to parse time out {0}. Seems to be in wrong order or repeating a unit.".format(timeout))
                          continue
                  else:
                      timeout = 0

                  self.nft_single(action, ip, topic, timeout)

                  # We re-sync our list of banned ips every 48 hours -
                  # sometimes due to network problems we may not have the same
                  # list as everyone else.
                  hours = 48
                  sync_cut_off = datetime.datetime.now() - datetime.timedelta(hours=hours)
                  if self.last_sync is None or self.last_sync < sync_cut_off:
                      # Ensure we don't sync again for another sync interval (something might
                      # go wrong and we may not fully sync, but it's better to wait 48 hours
                      # and try again compared with an endless loop of trying to re-sync and
                      # failing.
                      self.last_sync = datetime.datetime.now()
                      self.log("Last sync over {0} hours ago, re-sycing".format(hours))
                      self.request_sync()

        except KeyboardInterrupt:
            self.log("Cancelled by keyboard")
            self.cleanup()

def main():
  b = BruceBanner()
  b.setup()
  b.listen()

if __name__ == "__main__":
  sys.exit(main())

