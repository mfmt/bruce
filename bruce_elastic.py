#!/usr/bin/env python3

import asyncio
import datetime
import requests
import json
import os
import sys
import socket
import copy
import yaml
import argparse

# Define our priorities.
P_ERROR = "error"
P_DEBUG = "debug"

# Check environment variables.
BRUCE_ELASTIC_HOST = os.getenv('BRUCE_ELASTIC_HOST', None)
BRUCE_ELASTIC_USER = os.getenv('BRUCE_ELASTIC_USER', "bruce")
BRUCE_ELASTIC_PASSWORD = os.getenv('BRUCE_ELASTIC_PASSWORD', None)
BRUCE_ELASTIC_RULES_DIR = os.getenv('BRUCE_ELASTIC_RULES_DIR', '/etc/bruce-elastic-rules.d')

class BruceElastic:
    # Define object for holding found IPs so we don't re-ban IPs we just banned
    found = {}
    elastic_host = None
    elastic_user = None
    elastic_password = None
    elastic_rules_dir = None
    dry_run = False
    debug = False
    conf = {}

    def log(self, msg, priority=P_ERROR):
        # In debug mode, print all messages, otherwise only print P_ERROR messages.
        if self.debug == 1 or priority == P_ERROR:
            print (msg, flush=True)

    def purge_found(self, topic):
        # We should periodically do garbage collection on the found
        # dictionary or it will just get bigger and bigger.
        expire = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(minutes=10)
        ips_to_purge = []
        if topic in self.found:
            for ip in self.found[topic]:
                if self.found[topic][ip] < expire:
                    ips_to_purge.append(ip)
            self.log("Purging {0} ips from {1} topic found dictionary.".format(len(ips_to_purge), topic), P_DEBUG)
            for ip in ips_to_purge:
                del(self.found[topic][ip])

    def exonerate_ips(self, ips):
        # If we have more than 10,000 ips (which is rare), we break it up into chunks of
        # 10,000 to avoid exceeding our limits.
        limit = 10000
        chunked_list = list()
        if len(ips) > limit:
            for i in range(0, len(ips), limit):
                chunked_list.append(items[i:i+limit])
        else:
            chunked_list = [ ips ]


        for chunked_ips in chunked_list:
            url = self.elastic_host + self.conf["query_index"] + '/_search'

            exonerate_query = copy.deepcopy(self.conf["exonerate_query"])
            exonerate_query["query"]["bool"]["filter"].append({
                "terms": {
                    "dissect.client_ip": chunked_ips
                }
            })
            exonerate_query["aggs"] = {
                "ips": { "terms": { "field": "dissect.client_ip", "size": len(chunked_ips) } }
            }

            r = self.query_elastic(url, exonerate_query)
            results = r.json()
            try:
                for hit in results["aggregations"]["ips"]["buckets"]:
                    ip = hit["key"]
                    if ip in chunked_ips:
                        self.log("Exonerating IP {0}.".format(ip), P_DEBUG)
                        ips.remove(ip)
            except KeyError as e:
                self.log("Key error when getting exonerate results, possibly no results?")

        return ips


    def ban_found_ips(self, ips):
        if "exonerate_query" in self.conf:
            ips = self.exonerate_ips(ips)

        topic = self.conf["topic"]
        # Often, an IP will get logged on multiple servers at just about
        # the same time. We don't want to bother the bruce-server with
        # multiple requests to ban the same IP, so if we have already
        # banned an IP within the last 10 minutes (the minimum bruce ban
        # time is 10 minutes) we don't do it again.
        now = datetime.datetime.now(datetime.timezone.utc)
        expire = now - datetime.timedelta(minutes=10)

        for ip in ips:
            if not topic in self.found:
                self.found[topic] = {}
            if ip in self.found[topic] and self.found[topic][ip] > expire:
                # Don't ban it again.
                self.log("IP {0} in topic {1} already banned.".format(ip, topic), P_DEBUG)
                continue
            self.found[topic][ip] = now
            self.ban_ip(ip, topic)

    def ban_ip(self, ip, topic):
        self.log("Banning {0}, topic {1}".format(ip, topic), P_DEBUG)
        cmd = "add {0} {1}".format(ip, topic)
        if self.dry_run:
            self.log("In dry run mode, not banning.", P_DEBUG)
            return

        socket_paths = [ 'bruce.sock', '/run/bruce/bruce.sock']
        socket_path = None
        for try_socket_path in socket_paths:
            if os.path.exists(try_socket_path):
              # We found one.
              socket_path = try_socket_path
              break
        if not socket_path:
          self.log("Failed to find socket path, is bruce-server running?")
          sys.exit(1)

        client = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        try:
            client.connect(socket_path)
        except ConnectionRefusedError:
            self.log("Can't connect to bruce-server. Is it running?")
            sys.exit(1)
        client.send(cmd.encode('utf-8'))
        while True:
            datagram = client.recv(1024)
            if not datagram:
                self.log("Empty response from server, something went wrong.")
            else:
                response = datagram.decode('utf-8')
                if response != 'OK':
                    self.log("Error: {0}".format(response))
                else:
                    self.log("IP {0} in topic {1} banned successfully.".format(ip, topic), P_DEBUG)
            break

    def check_database(self):
        now = datetime.datetime.now(datetime.timezone.utc)
        some_minutes_ago = now - datetime.timedelta(seconds=self.conf['count_over_span'])

        # Convert to a format useable by elasticsearch.
        lte = now.replace(microsecond=0).isoformat()
        gte = some_minutes_ago.replace(microsecond=0).isoformat()

        ban_query = copy.deepcopy(self.conf['ban_query'])
        date_range_filter = {
            "range": {
              "@timestamp": {
                "gte": "now-{0}s".format(self.conf['count_over_span']),
                "lt": "now"
              }
            }
        }
        ban_query["query"]["bool"]["filter"].append(date_range_filter)
        ban_query["size"] = 10000
        ban_query["aggs"] = {
            "groupby": {
                "composite": {
                    "size":10000,
                    "sources": [
                        {
                            "client_ip": {
                                "terms": {
                                    "field": self.conf["ip_field_name"]
                                }
                            }
                        }
                    ]
                },
                "aggs": {
                    "client_ip": {
                        "filter": {
                            "exists": {
                                "field": self.conf["ip_field_name"]
                            }
                        },
                        "aggs": {
                            "count": {
                                "value_count": {
                                    "field": self.conf["ip_field_name"]
                                }
                            }
                        }
                    },
                    "having": {
                        "bucket_selector": {
                            "buckets_path": {
                                "count_path":"client_ip>count"
                            },
                            "script": {
                                "source":"params.count_path>{0}".format(self.conf['ban_counts_greater_than']),
                                "lang":"painless"
                            },
                            "gap_policy":"skip"
                        }
                    }
                }
            }
        }

        ips = []

        if not self.elastic_host:
            self.log("The BRUCE_ELASTIC_HOST environment variable is not set.")
            return ips

        url = self.elastic_host + self.conf["query_index"] + '/_search'

        if self.debug:
            query_start = datetime.datetime.now(datetime.timezone.utc)

        self.log("Running check_database for rule {0} between {1} and {2}".format(self.conf['name'], gte, lte), P_DEBUG)
        r = self.query_elastic(url, ban_query)
        json_result = r.json()
        if self.debug:
            query_end = datetime.datetime.now(datetime.timezone.utc)
            query_process_time = query_end - query_start
            self.log("Elastic query time for rule {0}: {1}".format(self.conf['name'], query_process_time.total_seconds()), P_DEBUG)

        if "hits" not in json_result or "total" not in json_result["hits"] or "value" not in json_result["hits"]["total"]:
            self.log("Failed to get proper structure of json output: {0}.".format(r.json()))
            return ips
        try:
            for bucket in json_result["aggregations"]["groupby"]["buckets"]:
                ip = bucket["key"]["client_ip"]
                if ip != "::1" and ip != "127.0.0.1":
                    ips.append(ip)
        except KeyError as e:
            self.log("Key error when getting ban results, possibly no results?")

        self.log("{0} hits for {1} rule.".format(len(ips), self.conf["name"]), P_DEBUG)
        return ips

    def query_elastic(self, url, query):

        headers = { 'Content-Type': 'application/json' }
        try:
            r = requests.get(url, headers=headers, data=json.dumps(query), auth=(self.elastic_user, self.elastic_password))
        except Exception as e:
            self.log("There was an error querying the elastic search server: {0}.".format(e))
            sys.exit(1)
        try:
            status_code = r.status_code
        except AttributeError:
            self.log("The call to requests did not return a request object with a status_code.")
            sys.exit(1)

        if status_code != 200:
            try:
                error = r.json()
            except json.decoder.JSONDecodeError:
                # If the elasticsearch server is really borked there is no telling what it will return.
                error = r.text
            self.log("Failed to get a 200 status code from elastic search. Received code: {0} and text: {1}.".format(status_code, error))
            self.log("query: {0}".format(query), P_DEBUG)
            sys.exit(1)
        return r


def main():
    # Parse command line arguments. We should only have command line arguments if
    # we are running in test mode.
    parser = argparse.ArgumentParser(description='Search for indications of intrusion detection in elasticsearch and ban offending IPs.')
    parser.add_argument('--test', help='Test the provided yaml rules file. Implies both debug and dry-run.', default=None, type=str)
    parser.add_argument('--debug', help='Output debugging info.', default=False, action='store_true')
    parser.add_argument('--dry-run', help='Do not ban any ips.', default=False, action='store_true')
    args = parser.parse_args()


    # Check for --test argument. If we find it, we short circuit the whole program
    # logic and simply run a test on the given file.
    if args.test:
        if not os.path.exists(args.test):
            print("The file {0} doesn't exist.".format(args.test))
            sys.exit(1)
        with open(args.test) as f:
            print("Loading rule file {0}.".format(args.test))
            conf = yaml.load(f, Loader=yaml.FullLoader)
        be = BruceElastic()
        # test mode implies both debug and dry run
        be.debug = True
        be.dry_run = True
        be.elastic_host = BRUCE_ELASTIC_HOST
        be.elastic_user = BRUCE_ELASTIC_USER
        be.elastic_password = BRUCE_ELASTIC_PASSWORD
        be.conf = conf
        ips = be.check_database()
        if ips:
            be.ban_found_ips(ips)
        sys.exit()

    # After reading the yaml files, we should end up with a list of rule
    # configurations.
    rules = []

    for file_name in os.listdir(BRUCE_ELASTIC_RULES_DIR):
        yaml_path = BRUCE_ELASTIC_RULES_DIR + "/" + file_name
        if yaml_path.endswith(".yml") or yaml_path.endswith(".yaml"):
            with open(yaml_path) as f:
                if args.debug:
                    print("Loading rule file {0}.".format(yaml_path))
                conf = yaml.load(f, Loader=yaml.FullLoader)
                rules.append(conf)

    # Setup the async run.
    loop = asyncio.get_event_loop()
    loop.set_exception_handler(exception_handler)
    for rule in rules:
      loop.create_task(run_loop(rule, args.debug, args.dry_run))
    try:
        loop.run_forever()
        sys.exit(1)
    except KeyboardInterrupt:
        print("Killed by cntrl-c.")
        sys.exit()

async def run_loop(conf, debug=False, dry_run=False):
    loop_count = 0
    if debug:
        print("Loading rule {0}.".format(conf["name"]))
    while True:
        loop_start = datetime.datetime.now(datetime.timezone.utc)
        be = BruceElastic()
        be.elastic_host = BRUCE_ELASTIC_HOST
        be.elastic_user = BRUCE_ELASTIC_USER
        be.elastic_password = BRUCE_ELASTIC_PASSWORD
        be.conf = conf
        be.debug = debug
        be.dry_run = dry_run
        ips = be.check_database()
        if ips:
            be.ban_found_ips(ips)
        if loop_count > 40:
            be.purge_found(topic=conf["topic"])
            loop_count = 0

        # We normally sleep for the configure Interval, but the time it took
        # to process this run means we might lose some time. So we adjust the
        # interval so account for the time it took to process the loop.
        loop_end = datetime.datetime.now(datetime.timezone.utc)
        loop_process_time = loop_end - loop_start
        sleep = conf["sleep_between_loops"] - loop_process_time.total_seconds()
        if debug:
            print("Interval for {0} rule is {1} and adjust sleep is: {2}.".format(conf["name"], conf["sleep_between_loops"], sleep))
        if sleep < 0:
            sleep = 0
        await asyncio.sleep(sleep)
        loop_count = loop_count + 1

# Ensure that we quit if we encounter any exceptions whatsover
# so we can detect that the program is not running and fix it.
def exception_handler(loop, context):
        print("Received an exception. Oh no!")
        loop.stop()
        raise context['exception']

if __name__ == "__main__":
        sys.exit(main())

