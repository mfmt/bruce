#!/usr/bin/python3

# bruce-server maintains the database of banned IPs and communicates with the
# servers that will be executing the actual bans. bruce receive commands via a
# socket (/run/bruce/bruce.sock).

import sys
import os
import shutil
import datetime
import socket
import zmq
import time
import requests
from IPy import IP
import dns.resolver
import yaml
import re
import signal

class BruceServer():
    mq_bind_ip = None
    mq_bind_port = None
    socket_path = None
    mq_socket = None
    db = None
    db_cursor = None
    db_type = None
    new_ignore_ips = []
    current_ignore_ips = []
    ignore_conf_path = None
    access_group = None
    placeholder = None

    def __init__(self):
        signal.signal(signal.SIGTERM, self.handle_term)

    def handle_term(self, signum, frame):
      self.log("Received TERM")
      self.db.commit()
      sys.exit(0)

    def log(self, msg):
        print(msg, flush=True)

    def setup(self):
        self.mq_bind_ip = os.getenv('BRUCE_SERVER_MQ_BIND_IP', '127.0.0.1')
        self.mq_bind_port = os.getenv('BRUCE_SERVER_MQ_BIND_PORT', '5556')
        self.socket_path = os.getenv('BRUCE_SERVER_SOCKET_PATH', 'bruce.sock')
        self.ignore_conf_path = os.getenv('BRUCE_SERVER_IGNORE_CONF_PATH', 'bruce-ignore.yml')
        self.access_group = os.getenv('BRUCE_ACCESS_GROUP', 'bruce')

        self.db_type = os.getenv("BRUCE_SERVER_DB_TYPE", 'sqlite3')
        if self.db_type == 'sqlite3':
            import sqlite3
            sqlite_path = os.getenv('BRUCE_SERVER_SQLITE_PATH', 'db.sqlite3')
            self.db = sqlite3.connect(sqlite_path)
            self.placeholder = '?'
        elif self.db_type == 'mysql':
            import pymysql
            connection_params = {
                'host': os.getenv('BRUCE_SERVER_MYSQL_HOST', 'localhost'),
                'db': os.getenv('BRUCE_SERVER_MYSQL_DB_NAME', 'bruce'),
                'user': os.getenv('BRUCE_SERVER_MYSQL_USER', 'bruce'),
                'password': os.getenv('BRUCE_SERVER_MYSQL_PASSWORD', None),
                'unix_socket': os.getenv('BRUCE_SERVER_MYSQL_UNIX_SOCKET', '/run/mysqld/mysqld.sock')
            }

            self.db = pymysql.connect(**connection_params)
            self.placeholder = '%s'

        self.db_cursor = self.db.cursor()

        # Ensure db tables are created.
        self.init()

        # Load ingore list.
        self.refresh_ignore()

        # Setup mq socket
        context = zmq.Context()
        self.mq_socket = context.socket(zmq.PUB)
        self.mq_socket.bind("tcp://{0}:{1}".format(self.mq_bind_ip, self.mq_bind_port))

    def validate_ip(self, ip):
        try:
            IP(ip)
        except:
            raise RuntimeError("That did not look like an IP address: {0}.".format(ip))

    def validate_topic(self, topic):
        if not re.match('^[a-z0-9]+$', topic):
            raise RuntimeError("Topics can only have letters and numbers.")

    def validate_host(self, host):
        if not re.match('^[a-z0-9-]+$', host):
            raise RuntimeError("Hosts can only have letters, numbers and dashes.")

    def listen(self):
        if os.path.exists(self.socket_path):
            os.remove(self.socket_path)

        self.log("Opening socket...")
        server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        server.bind(self.socket_path)
        self.log("Bound to {0}.".format(self.socket_path))

        # Ensure our socket is owned and writable by the bruce group
        # so members of the bruce group can send us commands.
        try:
          shutil.chown(self.socket_path, group=self.access_group)
        except LookupError as e:
          self.log("Failed to locate group: {0}. Please change BRUCE_ACCESS_GROUP or create the specified group.".format(self.access_group))
          return False
        os.chmod(self.socket_path, 0o660)

        # backlog is the number of connections we will allow to fill up while we
        # are processing requests. No idea what this should be - if we get slammed
        # we could get loads of add requests at the same time.
        backlog = 1000
        server.listen(backlog)
        while True:
            connection, client_address = server.accept()
            datagram = connection.recv(64)
            try:
                # Ignore empty requests.
                if not datagram:
                    continue
                else:
                    try:
                        args = datagram.decode('utf-8').split()
                        self.log("Received args {0}".format(args))
                        subcmd = args.pop(0)
                    except Exception as e:
                        raise RuntimeError("Failed to parse arguments: {0}".format(e))

                    self.log("Subcommand: {0}".format(subcmd))
                    if "add" == subcmd:
                        try:
                            ip = args[0]
                            topic = args[1]
                            self.validate_ip(ip)
                            self.validate_topic(topic)
                            self.add(ip, topic)
                        except IndexError:
                            raise RuntimeError("The ban command requires both ip and topic arguments")
                    elif "remove" == subcmd:
                        try:
                            ip = args[0]
                            self.validate_ip(ip)
                            topic = args[1]
                            self.validate_topic(topic)
                            self.remove(ip, topic)
                        except IndexError:
                            raise RuntimeError("The remove command requires an IP argument.")
                    elif "refresh-ignore" == subcmd:
                        self.refresh_ignore()
                    if "sync" == subcmd:
                        try:
                            host = args[0]
                            topic = args[1]
                            self.validate_host(host)
                            self.validate_topic(topic)
                            self.sync(host, topic)
                        except IndexError:
                            raise RuntimeError("The sync command requires both host and topic arguments")

                # Everything worked out, send back the OK message.
                connection.send("OK".encode('utf-8'))
            except Exception as e:
                if os.getenv('BRUCE_DEBUG', 0):
                    # In debug mode, crash and show backtrace.
                    raise e
                else:
                    # There was an error,
                    # We seem to sometimes get stuck in a state and the log reports:
                    # Error: (0, ''). I have no idea what it is - so let's try to
                    # trap that error and raise it so we can sort it out. Seems to be
                    # db related.
                    if "{0}".format(e) == (0, ''):
                        raise e
                    # Send it back to the client nicely.
                    connection.send("{0}".format(e).encode('utf-8'))
                    self.log("Error: {0}".format(e))

    # Mysql and sqlite use different placeholders. Sheesh. We have to run all queries
    # through this silliness to maintain compatibility between mysql and sqlite.
    # This is ridiculous. https://stackoverflow.com/questions/12184118/python-sqlite3-placeholder
    def set_ph(self, sql):
        ret = sql.replace(':ph', self.placeholder)
        return ret

    def init(self):
        # Ignore Table
        if self.db_type == 'mysql':
            self.db_cursor.execute("""
                CREATE TABLE IF NOT EXISTS ignored (
                    ip VARCHAR(64) PRIMARY KEY NOT NULL
                );
            """)

            # Ban table.
            self.db_cursor.execute("""
                CREATE TABLE IF NOT EXISTS banned (
                    ip VARCHAR(64) NOT NULL,
                    topic VARCHAR(64) NOT NULL,
                    ban_date VARCHAR(64) NOT NULL,
                    unban_date VARCHAR(64) NULL,
                    expire_date VARCHAR(64) NOT NULL,
                    ban_count INT DEFAULT 1,
                    PRIMARY KEY (ip, topic),
                    INDEX ban_date (ban_date),
                    INDEX expire_date (expire_date)
                );
            """)
        else:
            # sqlite3
            self.db_cursor.execute("""
                CREATE TABLE IF NOT EXISTS ignored (
                    ip TEXT PRIMARY KEY NOT NULL
                );
            """)

            # Ban table.
            self.db_cursor.execute("""
                CREATE TABLE IF NOT EXISTS banned (
                    ip TEXT KEY NOT NULL,
                    topic TEXT KEY NOT NULL,
                    ban_date TEXT KEY NOT NULL,
                    unban_date TEXT KEY NULL,
                    expire_date TEXT KEY NOT NULL,
                    ban_count INT DEFAULT 1,
                    PRIMARY KEY (ip, topic)
                );
            """)

        self.db.commit()

    def remove(self, ip, topic = None):
        self.log("Removing {0} in topic {1}".format(ip, topic))
        self.broadcast(topic, 'unban', ip)
        # Unban for 24 hours.
        unban_date = datetime.datetime.now() + datetime.timedelta(days=1)
        expire_date = datetime.datetime.now()
        if topic:
            self.db_cursor.execute(self.set_ph("UPDATE banned SET unban_date = :ph, expire_date = :ph, ban_count = 0 WHERE ip = :ph AND topic = :ph"), (unban_date, expire_date, ip, topic))
        else:
            self.db_cursor.execute(self.set_ph("UPDATE banned SET unban_date = :ph, expire_date = :ph, ban_count = 0 WHERE ip = :ph"), (unban_date, expire_date, ip))

        self.db.commit()

    def add(self, ip, topic):
        self.log("Adding {0} in topic {1}".format(ip, topic))
        if self.is_ignored(ip):
            raise RuntimeError("IP {0} is on ignore list, not adding.".format(ip))
            return

        existing = self.retrieve_ban(ip, topic)
        if existing:
            # They already exist in the database. Let's see if it's currently banned.
            expire_date = datetime.datetime.strptime(existing[0], '%Y-%m-%d %H:%M:%S.%f')
            ban_count = existing[1]
            unban_time = existing[2]
            if unban_time:
                unban_date = datetime.datetime.strptime(unban_time, '%Y-%m-%d %H:%M:%S.%f')
            else:
                unban_date = datetime.datetime.fromtimestamp(0)
            if expire_date > datetime.datetime.now():
                # They are banned, probably a timing issue. We do nothing.
                raise RuntimeError("Already banned {0} in topic {1}".format(ip, topic))
            if unban_date > datetime.datetime.now():
                # They are unbanned, don't reban them.
                raise RuntimeError("IP {0} in topic {1} has been unbanned".format(ip, topic))
        else:
            # They are brand new
            ban_count = 0

        # We are going to ban!
        ban_count = ban_count + 1
        # Figure out the expiration based on number of times we've been banned.
        expire_date = self.ban_count_to_expire_date(ban_count)
        # Convert expiration date to a time out (e.g. 4h23min).
        timeout = self.expire_date_to_timeout(expire_date)
        # Store this value in our database.
        self.insert(ip=ip, topic=topic, expire_date=expire_date, ban_count=ban_count)
        # Send it to all bruce-banners.
        self.broadcast(topic, 'ban', ip, timeout)

    # Broadcast all existing bans on the given topic to the given host. This function depends
    # on each bruce-banner client not only subscribing to the topic they want but also to
    # the host:topic they want (e.g. subscribe to both ssh and mailcf-001:ssh).
    def sync(self, host, topic):
        # Iterate over all existing bans.
        self.db_cursor.execute(
          self.set_ph("SELECT ip, expire_date FROM banned WHERE expire_date > :ph AND topic = :ph"),
            (datetime.datetime.now(), topic)
        )
        host_topic = "{0}:{1}".format(host, topic)
        bans = []
        for record in self.db_cursor.fetchall():
            ip = record[0]
            expire_date = datetime.datetime.strptime(record[1], '%Y-%m-%d %H:%M:%S.%f')
            # Convert expiration date to a time out (e.g. 4h23min).
            timeout = self.expire_date_to_timeout(expire_date)
            bans.append("{0} timeout {1}".format(ip, timeout))
        ban_set = ",".join(bans)
        self.broadcast(host_topic, 'sync', ban_set, timeout='')

    # Return True if the IP is in the ignore list, false otherwise.
    def is_ignored(self, ip):
        for ip_or_cidr in self.current_ignore_ips:
            if ip in IP(ip_or_cidr):
                return True
        return False

    # Attempt to retrieve an existing or previous ban for the given IP.  Return
    # a tuple (expire_date, ban_count, unban_date) if you find one or None if
    # there isn't one.
    def retrieve_ban(self, ip, topic):
        self.db_cursor.execute(
          self.set_ph("SELECT expire_date, ban_count, unban_date FROM banned WHERE ip = :ph AND topic = :ph"),
            [ip, topic]
        )
        return self.db_cursor.fetchone()

    # Given the ban count, what should the expiration be? Use an algorithm that
    # exponentially increases the ban time based on number of previous bans.
    def ban_count_to_expire_date(self, ban_count):
        expire = None
        now = datetime.datetime.now()
        if ban_count == 1:
            expire = now + datetime.timedelta(minutes=10)
        if ban_count == 2:
            expire = now + datetime.timedelta(minutes=20)
        if ban_count == 3:
            expire = now + datetime.timedelta(minutes=60)
        if ban_count == 4:
            expire = now + datetime.timedelta(minutes=120)
        if ban_count == 5:
            expire = now + datetime.timedelta(days=1)
        if ban_count == 6:
           expire = now + datetime.timedelta(days=2)
        if ban_count == 7:
            expire = now + datetime.timedelta(days=7)
        if ban_count > 7:
            expire = now + datetime.timedelta(weeks=4)

        return expire

    # Given a datetime object, convert it into a format
    # understood by nft (e.g 3d4h23s).
    def expire_date_to_timeout(self, expire_date):
        # Now convert it into the format understood by nft.
        delta = expire_date - datetime.datetime.now()
        seconds = delta.total_seconds()
        h = seconds // 3600
        m = (seconds%3600) // 60
        return "{0}h{1}m".format(int(h), int(m))

    # Insert the record into the database with the given expiration date.
    def insert(self, ip=None, topic=None, expire_date=None, ban_count=None):
        self.log("Inserting ip {0} with expire {1} in topic {2}".format(ip, expire_date, topic))
        self.db_cursor.execute(self.set_ph("REPLACE INTO banned(ip, topic, ban_date, expire_date, ban_count) VALUES (:ph, :ph, :ph, :ph, :ph)"),
            (ip, topic, datetime.datetime.now(), expire_date, ban_count))
        self.db.commit()

    # Send a single IP message out to all bruce-banners.
    def broadcast(self, topic, action, ip, timeout = 0 ):
        message = "{0} {1} {2} {3}".format(topic, action, ip, timeout)
        self.log("Sending: {0}".format(message))
        self.mq_socket.send_string(message)

    # The following functions are for building an ignore IP list.

    # If the individual IP is valid, add it to our self.new_ignore_ips list.
    def validate_and_add_ignore_ip(self, ip):
        self.new_ignore_ips.append(ip)

    def load_ignore_ips(self):
        self.db_cursor.execute( "SELECT ip FROM ignored" )
        for record in self.db_cursor.fetchall():
            self.current_ignore_ips.append(record[0])

    def save_ignore_ips(self):
        # Compare our new ignore list with our existing one and figure out
        # which ips to add and which to remove.
        delete = set(self.current_ignore_ips).difference(set(self.new_ignore_ips))
        add = set(self.new_ignore_ips).difference(set(self.current_ignore_ips))

        for ip in delete:
            self.log("Deleting Ignore IP: {0}".format(ip))
            self.db_cursor.execute(self.set_ph("DELETE FROM ignored WHERE ip = :ph"), (str(ip),) )
        for ip in add:
            self.log("Adding Ignore IP: {0}".format(ip))
            self.db_cursor.execute(self.set_ph("INSERT INTO ignored(ip) VALUES (:ph)"), (str(ip),) )
        self.db.commit()

    def parse_ignore_config(self):
        config = None
        if os.path.isfile(self.ignore_conf_path):
            with open(self.ignore_conf_path, 'r') as file:
                config_data = file.read(10000)
                config = yaml.safe_load(config_data)

        # If we don't have a config, no reason to continue.
        if not config:
          raise RuntimeError("Failed to find a configuration file.")

        return config

    # Refresh our list of IPs to ignore.
    def refresh_ignore(self):
        config = self.parse_ignore_config()

        if "domains" in config:
          for domain in config["domains"]:
            try:
                answers = dns.resolver.resolve(domain, 'A')
            except dns.resolver.NXDOMAIN:
                raise RuntimeError(f"Failed to resolve {domain}, check {self.ignore_conf_path}.")
            for data in answers:
              self.validate_and_add_ignore_ip(str(data))

        if "urls" in config:
          for url in config["urls"]:
            try:
                result = requests.get(url["address"])
            except requests.exceptions.ConnectionError:
                raise RuntimeError(f"Failed to connect to {url['address']}, check {self.ignore_conf_path}.")
            self.log(result.text)
            for line in result.text.split("\n"):
              if not line:
                continue
              try:
                self.validate_and_add_ignore_ip(line.decode('UTF-8'))
              except AttributeError:
                # If it's already a string, then great, just send it as is.
                self.validate_and_add_ignore_ip(line)

        if "ips" in config:
          for ip in config["ips"]:
            self.validate_and_add_ignore_ip(str(ip))

        # Special handler for deflect.
        if "deflect" in config:
            result = requests.get(config["deflect"]["address"])
            response = result.json()
            if not "list" in response:
                self.log("Failed to get deflect list. Maybe password is wrong?")
            else:
                for line in response["list"]:
                  try:
                    self.validate_and_add_ignore_ip(line.decode('UTF-8'))
                  except AttributeError:
                    # If it's already a string, then great, just send it as is.
                    self.validate_and_add_ignore_ip(line)

        # Load up the current ignore ips.
        self.load_ignore_ips()
        # Compare with new ones and update as needed.
        self.save_ignore_ips()
        # Reload.
        self.load_ignore_ips()
        self.db.commit()


def usage():
    print("""
bruce-server - ip ban manager

  bruce-server manages IP ban lists and communicates them to clients via
  ZeroMq. See bruce for commands you can send to bruce-server.

Usage
  bruce-server

Environment Variables and their defaults. See etc-default-bruce-server for and
explanation of each variable.
    BRUCE_SERVER_SOCKET_PATH=bruce.sock
    BRUCE_SERVER_IGNORE_CONF_PATH=bruce-ignore.yml
    BRUCE_SERVER_MQ_BIND_IP=127.0.0.1
    BRUCE_SERVER_MQ_BIND_PORT=5556
    BRUCE_SERVER_DB_TYPE=sqlite3
    BRUCE_SERVER_SQLITE_PATH=db.sqlite3
    BRUCE_SERVER_MYSQL_HOST=localhost
    BRUCE_SERVER_MYSQL_USER=bruce
    BRUCE_SERVER_MYSQL_DB_NAME=bruce
    BRUCE_SERVER_MYSQL_PASSWORD=
    BRUCE_DEBUG=0
""")

def main():
  b = BruceServer()
  b.setup()
  if not b.listen():
    usage()

if __name__ == "__main__":
  sys.exit(main())


