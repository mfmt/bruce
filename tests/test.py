#!/usr/bin/python3

# Test bruce.

import unittest
import uuid
import os
import subprocess
import grp
import time
import threading
import signal
import socket
import sqlite3
from bruce_elastic import BruceElastic
import yaml
import mock
import json

"""
Used to mock requests to elasticsearch server.
"""
def mocked_requests_get(*args, **kwargs):
  class MockResponse:
    def __init__(self, json_data, status_code):
      self.json_data = json_data
      self.status_code = status_code

    def json(self):
      return json.loads(self.json_data)

  data = json.loads(kwargs['data'])
  # We have to serve a different json file depending on whether this is the ban
  # query or the exonerate query. We're given the query - so we should for the
  # "should" key which only exists in the exonerate query.
  if "should" in data["query"]["bool"]:
    with open("tests/data/elastic.exonerate.out.json", "r") as f:
        raw_data = f.read()
  else:
    with open("tests/data/elastic.ban.out.json", "r") as f:
      raw_data = f.read()
  return MockResponse(raw_data, 200)

class BruceTestCase(unittest.TestCase):
  sqlite_path = None
  socket_path = None
  bruce_server = None
  bruce_banner = None
  t1 = None
  t2 = None

  def setUp(self):
    # Lauch bruce server. Use Popen so we don't block.
    self.sqlite_path = "/tmp/{0}.db".format(str(uuid.uuid4()))
    self.socket_path = "/tmp/{0}.sock".format(str(uuid.uuid4()))

    self.t1 = threading.Thread(target=self.launchServer)
    self.t2 = threading.Thread(target=self.launchBanner)
    self.t1.start()
    time.sleep(1)
    self.t2.start()
    time.sleep(1)

  def launchServer(self):
    gid = os.getgid()
    group_info = grp.getgrgid(gid)
    group_name = group_info.gr_name
    env = {
        "BRUCE_SERVER_DB_TYPE": "sqlite3",
        "BRUCE_SERVER_SQLITE_PATH": self.sqlite_path,
        "BRUCE_SERVER_SOCKET_PATH": self.socket_path,
        "BRUCE_ACCESS_GROUP": group_name,
        "BRUCE_SERVER_IGNORE_CONF_PATH": "config/bruce-ignore.yml"
    }
    self.bruce_server = subprocess.Popen(["{0}/bruce_server.py".format(os.getcwd())], env=env, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)

  def launchBanner(self):
    env = {
      "BRUCE_BANNER_SUBSCRIBE_TOPICS": "mail:110,143,587,465,993,995 ssh:22 web:443,80",
      "BRUCE_BANNER_CONNECT_PORT": "5556",
      "BRUCE_DEBUG": "1",
    }
    # Launch the bruce client in debug mode.
    self.bruce_banner = subprocess.Popen(["{0}/bruce_banner.py".format(os.getcwd())], stdout=subprocess.PIPE, env=env, text=True)

  def tearDown(self):
    if self.bruce_server.poll() is None:
        self.bruce_server.terminate()
        self.bruce_server.communicate()
    if self.bruce_banner.poll() is None:
        self.bruce_banner.terminate()
        self.bruce_banner.communicate()

    if os.path.exists(self.socket_path):
      os.remove(self.socket_path)
    if os.path.exists(self.sqlite_path):
      os.remove(self.sqlite_path)

  def assertLineInOutput(self, needle, haystack, name, count=1):
    found = False
    seenCount = 0
    for line in haystack:
      if line.startswith(needle):
        seenCount += 1
    self.assertEqual(count, seenCount, name)

  def assertLineNotInOutput(self, needle, haystack, name):
    found = False
    seen = 0
    for line in haystack:
      if line == needle:
        print("Found: " + line)
        found = True
        break
    self.assertFalse(found, name)

  def testBanIp(self):
    ip_that_should_be_banned = '10.0.0.2'
    ip_that_should_be_ignored = '192.168.0.117'
    env = {
        "BRUCE_SERVER_SOCKET_PATH": self.socket_path
    }
    # Refresh the ignore list.
    subprocess.run(["{0}/bruce.py".format(os.getcwd()), "refresh-ignore"], env=env, stdout=subprocess.DEVNULL)
    # Ban an address that should be banned.
    subprocess.run(["{0}/bruce.py".format(os.getcwd()), "add", ip_that_should_be_banned, "web"], env=env, stdout=subprocess.DEVNULL)
    # Ban an address that should not be banned (ignore list).
    subprocess.run(["{0}/bruce.py".format(os.getcwd()), "add", ip_that_should_be_ignored, "web"], env=env, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    # Terminate all processes, collect output.
    self.bruce_server.terminate()
    self.bruce_server.communicate()
    self.bruce_banner.terminate()
    output = self.bruce_banner.communicate()[0].split("\n")

    # Ensure we banned the right IP
    want = "['sudo', 'nft', 'add', 'element', 'inet', 'bruce', 'bruce-web', '{{ {0} timeout 0h9m }}']".format(ip_that_should_be_banned)
    self.assertLineInOutput(want, output, name="Ban a single IP")

    # Ensure we did not ban the wrong IP
    not_want = "['sudo', 'nft', 'add', 'element', 'inet', 'bruce', 'bruce-web', '{{ {0} timeout 0h9m }}']".format(ip_that_should_be_ignored)
    self.assertLineNotInOutput(not_want, output, name="Do not ban ignored IP")

    # Check the database.
    db = sqlite3.connect(self.sqlite_path)
    cursor = db.cursor()

    cursor.execute("SELECT COUNT(*) AS count FROM banned WHERE ip = ? AND topic = 'web'", (ip_that_should_be_banned,))
    found_count = cursor.fetchone()[0]
    self.assertEqual(found_count, 1, "Banned IP address is inserted into database.")

    cursor.execute("SELECT COUNT(*) AS count FROM banned WHERE ip = ? AND topic = 'web'", (ip_that_should_be_ignored,))
    found_count = cursor.fetchone()[0]
    self.assertEqual(found_count, 0, "Ignore IP address is NOT inserted into database.")

  def testUnbanIp(self):
    env = {
        "BRUCE_SERVER_SOCKET_PATH": self.socket_path
    }
    # Ban an address that should be banned.
    subprocess.run(["{0}/bruce.py".format(os.getcwd()), "add", "192.0.2.51", "ssh"], env=env, stdout=subprocess.DEVNULL)

    # Unban the address.
    subprocess.run(["{0}/bruce.py".format(os.getcwd()), "remove", "192.0.2.51", "ssh"], env=env, stdout=subprocess.DEVNULL)

    # Try to reban it (it should not get re-banned).
    subprocess.run(["{0}/bruce.py".format(os.getcwd()), "add", "192.0.2.51", "ssh"], env=env, stdout=subprocess.DEVNULL)

    # Terminate all processes, collect output.
    self.bruce_server.terminate()
    self.bruce_server.communicate()
    self.bruce_banner.terminate()
    output = self.bruce_banner.communicate()[0].split("\n")

    # Ensure we banned the IP only once.
    want = "['sudo', 'nft', 'add', 'element', 'inet', 'bruce', 'bruce-ssh', '{ 192.0.2.51 timeout 0h9m }']"
    self.assertLineInOutput(want, output, name="After unban, do not ban again.", count=1)

    # Check the database.
    db = sqlite3.connect(self.sqlite_path)
    cursor = db.cursor()

    cursor.execute("SELECT unban_date FROM banned WHERE ip = '192.0.2.51' AND topic = 'ssh'")
    unban_date = cursor.fetchone()[0]
    self.assertIsNotNone(unban_date, "Unbanned IP address is inserted into database with unban date.")

  def testSync(self):
    env = {
        "BRUCE_SERVER_SOCKET_PATH": self.socket_path
    }
    subprocess.run(["{0}/bruce.py".format(os.getcwd()), "add", "192.0.2.50", "web"], env=env, stdout=subprocess.DEVNULL)
    subprocess.run(["{0}/bruce.py".format(os.getcwd()), "add", "192.0.2.51", "web"], env=env, stdout=subprocess.DEVNULL)
    subprocess.run(["{0}/bruce.py".format(os.getcwd()), "sync", socket.gethostname(), "web"], env=env, stdout=subprocess.DEVNULL)
    self.bruce_server.terminate()
    self.bruce_server.communicate()
    self.bruce_banner.terminate()
    output = self.bruce_banner.communicate()[0].split("\n")
    # Not a great test here - just rudimentary check to make sure we are calling nft with the --file
    # command. However, in debug mode bruce-banner will actually execute an nft command that passes
    # the --file argument using the --check option, and it will check for syntax errors and fail if
    # there is a syntax error, so this simple check ensures that the more effective check has in fact
    # run.
    want = "['/usr/sbin/nft', '--file'"
    self.assertLineInOutput(want, output, name="Sync IPs", count=1)
    self.assertEqual(0, self.bruce_server.returncode, "Bruce Server terminated normally")
    self.assertEqual(0, self.bruce_banner.returncode, "Bruce Banner terminated normally")


  @mock.patch('requests.get', side_effect=mocked_requests_get)
  def testBruceElastic(self, mock_get):
    # This is not yet working.
    # Sleep so our processes that are started have time to start before we shut them
    # down.
    time.sleep(2)
    # It doesn't really matter which rules file we use, we are mocking the request to
    # elastic search so we always get the same results. In other words, we are not
    # testing the rules definition, we are only testing the bruce-elastic code that
    # parses the results.
    with open("config/bruce-elastic-rules.d/sample.yml") as f:
      conf = yaml.load(f, Loader=yaml.FullLoader)
    be = BruceElastic()
    be.debug = False
    be.dry_run = False
    be.elastic_host = "https://elastic.example.org/"
    be.elastic_user = "no-login"
    be.elastic_password = "happens-when-testing"
    be.conf = conf
    initial_ips = be.check_database()
    want = ['10.132.193.115', '10.36.99.12', '10.36.93.15']
    self.assertEqual(want, initial_ips, "Bruce elastic picks right IPs to ban.")
    post_exonerate_ips = be.exonerate_ips(initial_ips)
    want = ['10.36.99.12', '10.36.93.15']
    self.assertEqual(want, post_exonerate_ips, "Bruce elastic properly exonerates.")


